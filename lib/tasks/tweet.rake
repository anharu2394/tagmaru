namespace :tweet do
    desc "ツイートする"
    task :tweet => :environment do
        if Rails.env == "development"
            client = Twitter::REST::Client.new do |config|
                config.consumer_key = ENV["TWITTER_KEY"]
                config.consumer_secret = ENV["TWITTER_SECRET"]
                config.access_token = ENV["ACCESS_TOKEN"]
                config.access_token_secret = ENV["ACCESS_SECRET"]
            end
        elsif Rails.env == "production"
            client = Twitter::REST::Client.new do |config|
                config.consumer_key = Rails.application.secrets.twitter_api_key
                config.consumer_secret = Rails.application.secrets.twitter_api_secret
                config.access_token = Rails.application.secrets.twitter_access_token
                config.access_token_secret = Rails.application.secrets.twitter_access_secret
            end
        end
        tweet = "今日のたぐまるのトレンドにゃん！\n"
        Post.where(posted_at: Date.today - 5..Date.today).order("fab_count desc").limit(5).each do |post|
            tweet = tweet + post.title + "\n"  
        end
        if  tweet.length >= 108
            tweet = tweet[0..107]
        end
        tweet = tweet + "... \n #tagmaru \n tagmaru.me"
        puts tweet.length
        begin
            client.update(tweet)
        rescue => e
            client.update("たぐまるトレンド定期ツイートで問題が発生したにゃん \n　ごめんなさいにゃん" + e.message)
        end
    end
end
