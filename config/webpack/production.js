process.env.NODE_ENV = process.env.NODE_ENV || 'production'
const environment = require('./environment')
const UglifyJsPlugin = environment.plugins.get('UglifyJs');
UglifyJsPlugin.options.sourceMap = false;
module.exports = environment.toWebpackConfig()
