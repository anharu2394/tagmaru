Rails.application.routes.draw do
  get '/mypage',to:'static#index'
  get '/auth/:provider/callback', to: 'sessions#create'
  get '/logout', to: 'sessions#destroy'
  get '/login',to:'static#index'
  get '/tags/:id',to:'static#index'
  scope "api" do
    resources :tags, only:[:index, :show] do
        get :search, on: :collection
    end
    resources :follow_tags, only:[:create, :destroy]
    get '/login_user', to: "sessions#login_user"
    get '/users/current', to:"users#current"
    get '/users/:user_id/tags', to: "tags#user_index"
    get '/posts/:tag_id/:type',to: "posts#index"
    get '/posts/trend', to: "posts#trend"
  end
  root 'static#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
