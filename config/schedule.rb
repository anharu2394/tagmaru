# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron
rails_env = ENV['RAILS_ENV'] || :development
set :environment, rails_env

# Example:
#
set :output, "log/crontab.log"
env :PATH, ENV['PATH']
job_type :rbenv_rake, %q!eval "$(rbenv init -)"; cd :path && :environment_variable=:environment bundle exec rake :task --silent :output!
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
every 1.day, :at => '4:00 am' do
     rbenv_rake "post_save:qiita"
     rbenv_rake "post_save:devto"
     rbenv_rake "post_save:hateb"
     rbenv_rake "post_save:qrunch"
end
 every 1.day, :at => '10:00 am' do
     rbenv_rake "tweet:tweet"
end
 every 1.day, :at => '4:00 pm' do
     rbenv_rake "tweet:tweet"
end
 every 1.day, :at => '8:00 pm' do
     rbenv_rake "tweet:tweet"
end
