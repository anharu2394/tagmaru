import axios from 'axios'
export const REQUEST_FETCH_TAGS = 'REQUEST_FETCH_TAGS'
export const FETCH_TAGS = 'FETCH_TAGS'
export const FETCH_FOLLOW_TAGS = 'FETCH_FOLLOW_TAGS'
export const FETCH_USER = 'FETCH_USER'
export const FETCH_TAG_DETAILS = 'FETCH_TAG_DETAILS'
export const CHANGE_FOLLOW_TAG = 'CHANGE_FOLLOW_TAG'
export const FETCH_POSTS = 'FETCH_POSTS'
export const FETCH_TREND_POSTS = 'FETCH_TREND_POSTS'
export const CHANGE_POST_TAB = 'CHANGE_POST_TAB'
export const SEND_MESSAGE = 'SEND_MESSAGE'
const Actions = {
    requestFetchTags() {
        return {
            type: REQUEST_FETCH_TAGS
        }
    },
    fetchTags(result) {
        return {
            type: FETCH_TAGS,
            ...result,
        }
    },
    fetchFollowTags(result) {
        return {
            type: FETCH_FOLLOW_TAGS,
            ...result,
        }
    },
    fetchUser(result) {
        return {
            type: FETCH_USER,
            ...result,
        }
    },
    fetchTagDetails(result) {
        return {
            type: FETCH_TAG_DETAILS,
            ...result,
        }
    },
    changeFollowTag(result) {
        return {
            type: CHANGE_FOLLOW_TAG,
            ...result,
        }
    },
    changePostTab(result) {
        return {
            type: CHANGE_POST_TAB,
            ...result,
        }
    },
    fetchPosts(result) {
        return {
            type: FETCH_POSTS,
            ...result,
        }
    },
    fetchTrendPosts(result) {
        return {
           type: FETCH_TREND_POSTS,
            ...result,
        }
    },
    sendMessage(result) {
        return {
            type: SEND_MESSAGE,
            ...result,
        }
    }
}

export default Actions

