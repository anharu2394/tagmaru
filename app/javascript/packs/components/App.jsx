import React, { Component } from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'
import Header from './../layouts/header'
import Home from './../layouts/home'
import Footer from './../layouts/footer'
import MyPage from './../layouts/mypage'
import DetailTag from "./../layouts/detail_tag"
import Login from "./../layouts/login"
import TagSearch from "./../layouts/tag_search"
import Notification from './../components/notification'
import {  Link  }from 'react-router'
import { BrowserRouter,Route,Switch } from 'react-router-dom'

export default class App extends Component {
    constructor(props) {
        super(props)
    }
    componentWillMount() {
        this.props.loginUser()
        this.props.getTrendPosts()
    }
    render () {
        return (
            <div>
                <Header {...this.props} />
               	<Notification {...this.props} />
                <Switch>
                    <Route exact path="/" render={() => <Home {...this.props} />} />
                    <Route exact path="/mypage" render={() => <MyPage {...this.props}/>} />
                    <Route exact path="/login" render={() => <Login {...this.props} />} />
                    <Route path="/tags/search" render={() => <TagSearch {...this.props}/>} />
                    <Route path="/tags/:id" render={({match}) => <DetailTag {...this.props} match={match}/>} />
                </Switch>
                <Footer />
            </div>
        )
    }
}
