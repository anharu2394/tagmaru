import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import axios from 'axios'
import { BrowserRouter, Route, Link } from 'react-router-dom'

export default class Tag extends Component {
    constructor(props){
        super(props)
    }
    render() {
        const tag = this.props.tag
        const msg = this.props.tag.following ? "フォロー解除" : "フォローする"
        const classNameName = this.props.tag.following ? "button is-info is-small is-pulled-right is-outlined" : "button is-info is-small is-pulled-right"
        const clickFn = this.props.tag.following ? this.props.unfollowTag : this.props.followTag
        return(
            <div className="box"  >
            <p><Link to={"/tags/" + tag.id}>{tag.name}</Link><button onClick={(e) => clickFn(e,this.props.from)} id={tag.id}className={classNameName}>{msg}</button></p>
            </div>
        )
    }
}
