import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import axios from 'axios'

export default class PostLink extends Component {
    constructor(props){
        super(props)
    }
    render() {
        const post = this.props.post
        if (post.provider == "qiita") {
            const url = "https://qiita.com" + post.url
            return(
                <div className="box qiita" >
                    <a href={url} target="_blank">
                        {post.title}
                    </a>
                    <div>いいね数{post.fab_count}</div>
                    <span className="qiitaLabel">Qiita</span>
                </div>
            )
        }
        else if (post.provider == "devto") {
            const url = "https://dev.to" + post.url
            return(
                <div className="box devto" >
                    <a href={url} target="_blank">
                        {post.title}
                    </a>
                    <div>いいね数{post.fab_count}</div>
                    <span className="devtoLabel">Devto</span>
                </div>
            )
        }
        else if (post.provider == "hateb") {
            const url = post.url
            return(
                <div className="box hateb" >
                    <a href={url} target="_blank">
                        {post.title}
                    </a>
                    <p className="url">{ "(" + url + ")"}</p>
                    <div>いいね数{post.fab_count}</div>
                    <span className="hatebLabel">Any</span>
                </div>
            )
        }
        else if (post.provider == "qrunch") {
            const url = "https://qrunch.io" + post.url
            return(
                <div className="box qrunch" >
                    <a href={url} target="_blank">
                        {post.title}
                    </a>
                    <div className="qrunchLabel">Qrunch</div>
                </div>
            )
        }
        else {
            return(
                <div className="box" target="_blank">
                    <a href="">
                        {post.title}
                    </a>
                    <div>いいね数{post.fab_count}</div>
                    <span>None</span>
                </div>
            )
        }
    }
}
