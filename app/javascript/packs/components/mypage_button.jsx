import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import axios from 'axios'

export default class MypageButton extends Component {
    render() {
        if (this.props.logged_in){
            return(
                  <p className="control">
                    <a className="button is-info" href="/mypage">
                      <span className="icon">
                        <i className="far fa-address-card"></i>
                      </span>
                      <span>マイページ</span>
                    </a>
                  </p>
            )
        }
        else{
            return(
                  <p className="control">
                    <a className="button is-info" href="/auth/twitter">
                      <span className="icon">
                        <i className="fas fa-pencil-alt"></i>
                      </span>
                      <span>登録</span>
                    </a>
                  </p>
            )
        }
    }
}
