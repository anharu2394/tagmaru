import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import axios from 'axios'

export default class Hero extends Component {
    render(){
        return(
        <section className="hero">
          <div className="hero-body">
            <div className="container">
              <div className="columns">
                  <div className="column">
                  <h1 className="title">
                    {this.props.title}
                  </h1>
                  <h2 className="subtitle">
                    {this.props.subtitle}
                  </h2>
                  </div>
              </div>
            </div>
          </div>
        </section>
        )
    }
}
