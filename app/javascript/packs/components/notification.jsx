import React, {Component} from 'react'

const Notification = (props) => {
    const deleteMessage = () => {
        document.getElementsByClassName('notification')[0].remove()
    }
    if (props.error)　{
        return (
            <div className="container">
            <div className="notification is-danger">
                <strong>問題が発生しました</strong><br />
                {props.error.message}
                <button className="delete" aria-label="delete" onClick={deleteMessage}></button>
            </div>
            </div>
        )
    }
    else if (props.message) {
        return (
            <div className="container">
            <div className="notification is-info">
                <strong>{props.message}</strong>
                <button className="delete" aria-label="delete" onClick={deleteMessage}></button>
            </div>
            </div>
        )
    }
    else {
        return (
            <div />
        )
    }
}
export default Notification
