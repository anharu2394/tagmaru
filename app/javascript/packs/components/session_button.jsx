import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import axios from 'axios'

export default class SessionButton extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        const btn_text = this.props.logged_in ? "ログアウト" : "ログイン"
        const url = this.props.logged_in ? "/logout" : "/auth/twitter"
        if (this.props.logged_in){
            return (
              <p className="control">
                <a className="button is-primary" href={url}>
                  <span className="icon">
                    <i className="fas fa-sign-in-alt"></i>
                  </span>
                  <span>{btn_text}</span>
                </a>
              </p>
            )
        }
        else{
            return(
              <p className="control">
                <a className="button is-primary" href={url}>
                  <span className="icon">
                    <i className="fas fa-sign-in-alt"></i>
                  </span>
                  <span>{btn_text}</span>
                </a>
              </p>
            )
        }
    }
}
