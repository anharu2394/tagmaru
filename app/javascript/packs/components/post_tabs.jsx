import React from 'react'


const PostTabs = (props) => {
    let trendClass = (props.post_tab === "trend")?"is-active":""
    let latestClass = (props.post_tab === "latest")?"is-active":""
    let popuClass = (props.post_tab === "popularity")?"is-active":""


    return (
        <div className="tabs is-boxed is-fullwidth">
  <ul>
      <li className={trendClass} onClick={ () => props.changePostTab("trend")}>
      <a>
        <span>トレンド</span>
      </a>
        </li>
      <li className={latestClass} onClick={ () => props.changePostTab("latest")}>
      <a>
        <span>新着</span>
      </a>
        </li>
        <li className={popuClass}onClick={ () => props.changePostTab("popularity")}>
      <a>
        <span>人気</span>
      </a>
    </li>
  </ul>
</div>
    )
}
export default PostTabs
