import axios from 'axios'
import {
  connect,
} from 'react-redux'
import App from './../components/App'
import Actions from './../actions/AppActions'

const mapStateToProps = (state) => {
  return state
}
const mapDispatchToProps = (dispatch) => {
    return {
        getTags() {
            dispatch(Actions.requestFetchTags())
            axios.get("/api/tags")
                .then(res =>
                    dispatch(Actions.fetchTags({status:'success',response:res.data})))
                .catch(error =>
                    dispatch(Actions.fetchTags({status:'error',error:error}))
                )
        },
        loginUser() {
            axios.get("/api/login_user")
                .then(res =>{
                    dispatch(Actions.fetchUser({status:'success',response:res.data}))
                    if (res.data.logged_in === true) {
                        this.getTags()
                        this.getFollowTag(res.data.user.id)
                    }
                })
                .catch(error =>
                    dispatch(Actions.fetchUser({status:'error',error:error}))
                )
        },
        getFollowTag(id) {
                dispatch(Actions.requestFetchTags())
                axios.get("/api/users/" + id + "/tags")
                    .then(res =>
                        dispatch(Actions.fetchFollowTags({status:'success',response:res.data})))
                    .catch(error =>
                        dispatch(Actions.fetchFollowTags({status:'error',error:error}))
                    )
        },
        getTagDetails(id) {
            axios.get("/api/tags/" + id)
                .then(res =>
                    dispatch(Actions.fetchTagDetails({status:'success',response:res.data}))
                )
                .catch(error =>
                    dispatch(Actions.fetchTagDetails({status:'error',error:error}))
                )
        },
        followTag(e,from) {
                console.log(e.target.id)
                axios.defaults.headers['X-CSRF-TOKEN'] = document.getElementsByName('csrf-token')[0].getAttribute('content')
                axios.post("/api/follow_tags",{
                    tag_id:e.target.id
                })
                .then(res =>
                    dispatch(Actions.changeFollowTag({status:'success',response: {id:res.data.tag_id,from:from,following:true}}))
                ).catch(error =>
                    dispatch(Actions.changeFollowTag({status:'error',error:error}))
                )
        },
        unfollowTag(e,from) {
                console.log("un" + e.target.id)
                const id = e.target.id
                axios.defaults.headers['X-CSRF-TOKEN'] = document.getElementsByName('csrf-token')[0].getAttribute('content')
                axios.delete("/api/follow_tags/" + e.target.id)
                .then(res =>
                    dispatch(Actions.changeFollowTag({status:'success', response: {id:res.data.tag_id,from:from,following:false}}))
                ).catch(error =>
                    dispatch(Actions.changeFollowTag({status:'error',error:error}))
                )
        },
        getPosts(id) {
                axios.get("/api/posts/" + id + "/popular")
                    .then(res => {
                        dispatch(Actions.fetchPosts({status:'success',response:res.data,tab: "popularity"}))
                    })
                    .catch(error =>
                        dispatch(Actions.fetchPosts({status:'error',error:error}))
                    )
                axios.get("/api/posts/" + id + "/trend")
                    .then(res => {
                        dispatch(Actions.fetchPosts({status:'success',response:res.data,tab: "trend"}))
                    })
                    .catch(error =>
                        dispatch(Actions.fetchPosts({status:'error',error:error}))
                    )
                axios.get("/api/posts/" + id + "/latest")
                    .then(res => {
                        dispatch(Actions.fetchPosts({status:'success',response:res.data,tab: "latest"}))
                    })
                    .catch(error =>
                        dispatch(Actions.fetchPosts({status:'error',error:error}))
                    )
        },
        getTrendPosts() {
            axios.get("/api/posts/trend")
                .then(res =>
                    dispatch(Actions.fetchTrendPosts({status:"success", response: res.data.posts}))
                ).catch(error =>
                    dispatch(Actions.fetchTrendPosts({status: 'error', error:error}))
                )
        },
        changePostTab(post_tab) {
            dispatch(Actions.changePostTab({status:"success", response: post_tab}))
        },
        sendMessage(message) {
            dispatch(Actions.sendMessage({status:'success', response: message}))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(App)
