import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import axios from "axios"
import fetFollowTags from './../actions/AppActions'
import { Router, Route, IndexRoute, Link, IndexLink, browserHistory } from 'react-router'
import Tag from './../components/tag'
import PostLink from './../components/post_link'
import TagmaruImage from "images/tagmaru_maeashi.png"

export default class Home extends Component {
    constructor(props) {
        super(props)
        if (document.referrer.match(/api.twitter.com\/oauth/)) {
            props.sendMessage("ログイン、ありがとうございます！")
        }
    }
    render() {
        if (this.props.logged_in === true) {
            return (
                <section className="section">
                <div className="container">
                    <div className="columns">
                        <div className="column is-two-thirds">
                            <h1 className="title">トレンド</h1>
                            {
                                this.props.home_trend_posts.map((post) => <PostLink key={post.id} post={post} />)
                            }
                        </div>
                        <div className="column">
                            <h1 className="title">人気なタグ</h1>
                            {
                                this.props.tags.map((tag) => (<Tag {...this.props} tag={tag} key={tag.id} from="tags"/>))
                            }
                        </div>
                    </div>
                </div>
                </section>
            )
        }
        else if ( this.props.isFetching === true && this.props.logged_in === false){
            return (
                <div>
                <section className="hero is-medium">
                    <div className="hero-body">
                        <div className="container">
                            <div className="columns">
                                <div className="column is-two-thirds">
                                    <h1 className="title">たぐまるはエンジニアのための情報収集を快適にするサービスです。</h1>
                                </div>
                                <div className="column">
                                    <img src={TagmaruImage} />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                  <section className="section">
                    <div className="container">
                          <h1 className="title has-text-centered">すぐにTwitterで登録</h1>
                          <div className="buttons has-addons is-centered">
                            <a href="/auth/twitter" className="button is-info is-large is-rounded">Twitterで登録</a>
                          </div>
                    </div>
                  </section>
                  <section className="section">
                    <div className="container">
                          <h1 className="title">Trend</h1>
                           {
                                this.props.home_trend_posts.map((post) => <PostLink key={post.id} post={post} />)
                            }
                    </div>
                  </section>
                </div>
            )
        }
        else {
            return (
                <div>loading</div>
            )
        }
    }
}
