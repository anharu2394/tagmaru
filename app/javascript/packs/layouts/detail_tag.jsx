import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import axios from "axios"
import { Router, Route, IndexRoute, Link, IndexLink, browserHistory } from 'react-router'
import Hero from "./../components/hero"
import PostLink from "./../components/post_link"
import PostTabs from "./../components/post_tabs"

export default class DetailTag extends Component {
    constructor(props){
        super(props)
        props.getPosts(props.match.params.id)
        props.getTagDetails(props.match.params.id)
    }
    render() {
        const tag = this.props.detail_tag
        let posts
        if (this.props.post_tab === "trend") {
            posts = this.props.trend_posts
            console.log(this.props.trend_posts)
        }
        else if (this.props.post_tab === "popularity") {
            posts = this.props.popu_posts
        }
        else if (this.props.post_tab === "latest") {
            posts = this.props.latest_posts
        }
        else {
          posts = []
        }
        console.log(posts)
        console.log(this.state)
        return (
            <div className="container">
                <Hero title={tag.name}></Hero>
            <PostTabs {...this.props}/>
                {posts.map((post) => (<PostLink key={post.id} post={post} />))}
            </div>
        )
    }
}
