import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { Router, Route, IndexRoute, Link, IndexLink, browserHistory } from 'react-router'
import SessionButton from './../components/session_button'
import MypageButton from './../components/mypage_button'
import Notification from './../components/notification'
import Logo from 'images/tagmaru_logo.jpg'

export default class Header extends Component {
    constructor(props){
        super(props)
    }
    listener() {
        const element = document.querySelector('#navbar-toggle')
        element.addEventListener('click', (e) => {
            e.target.classList.toggle('is-active')
            document.querySelector('#navbar-menu').classList.toggle('is-active')
        })
    }
    componentDidMount() {
        this.listener()
    }
    render() {
        return (
            <nav className="navbar is-transparent">
  <div className="navbar-brand">
    <a className="navbar-item" href="/">
      <img src={Logo} alt="Logo" />
    </a>
    <div className="navbar-burger burger" id="navbar-toggle">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>

  <div className="navbar-menu" id="navbar-menu">
    <div className="navbar-start">
      <a className="navbar-item" href="/">
        Home
      </a>
        {(() => {
        return this.props.logged_in ? <a className="navbar-item" href="/tags/search">タグ検索</a>:<a />})()}
    </div>

    <div className="navbar-end">
      <div className="navbar-item">
        <div className="field is-grouped">
          <p className="control">
            <a className="bd-tw-button button" data-social-network="Twitter" data-social-action="tweet" data-social-target="http://tagmaru.me" target="_blank" href="https://twitter.com/intent/tweet?text=エンジニアのための情報収集サービス「たぐまる」&amp;hashtags=tagmaru&amp;url=http://tagmaru.me&amp;via=_anharu2">
              <span className="icon">
                <i className="fab fa-twitter"></i>
              </span>
              <span>
                ツイートで共有
              </span>
            </a>
          </p>
          <MypageButton logged_in={this.props.logged_in}/>
          <SessionButton logged_in={this.props.logged_in}/>
        </div>
      </div>
    </div>
  </div>
</nav>
        )
    }
}
