import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import axios from "axios"
import { Router, Route, IndexRoute, Link, IndexLink, browserHistory } from 'react-router'
import Tag from "./../components/tag"
import Hero from "./../components/hero"

 export default class TagSearch extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tags:[]
        }
    }
    search(e) {
        axios.get("/api/tags/search?keyword=" + e.target.value).then(res => {
            this.setState({
                tags: res.data
            })
        })
    }
     render() {
         return (
             <div className="container">
                    <Hero title="タグ検索" />
                    <div className="level">
                        <div className="level-left">
                            <div className="leve-item has-text-centered">
                                <p>検索ワードを入力</p>
                            </div>
                            <div className="leve-item has-text-centered">
                                <input type="text" className="input" onChange={this.search.bind(this)} />
                            </div>
                        </div>
                    </div>
                    {
                        this.state.tags.map((tag) => (<Tag {...this.props}key={tag.id}tag={tag} from="tag_search"/>))
                    }
             </div>
         )
     }
 }
