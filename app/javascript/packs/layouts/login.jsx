import React from "react"
import SessionButton from './../components/session_button'

const Login = (props) => (
    <div className="container">
        <div className="section">
            <h1 className="title">ログイン</h1>
        </div>
    	  <SessionButton logged_in={props.logged_in}/>
    </div>
)

export default Login
