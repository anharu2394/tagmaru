import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { Router, Route, IndexRoute, Link, IndexLink, browserHistory } from 'react-router'

export default class Footer extends Component {
    render () {
        return (
            <footer className="footer">
            <div className="container">
                <div className="content has-text-centered">
                    <p>
                      <strong>Developed</strong> by <a href="https://twitter.com/_anharu2">Anharu</a>
                    </p>
                    <p>
                      <strong>Designed</strong> by <a href="">Yuki</a> and my best frend, thank you.
                    </p>
                </div>
            </div>
            </footer>
        )
    }
}
