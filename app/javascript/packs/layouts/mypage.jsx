import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import axios from "axios"
import fetFollowTags from './../actions/AppActions'
import { Router, Route, IndexRoute, Link, IndexLink, browserHistory } from 'react-router'
import Tag from "./../components/tag"

export default class MyPage extends Component {
    constructor(props) {
        super(props)
        console.log(props)
        this.state = {
            tags: []
        }
    }
    render() {
        return(
            <div>
            <section className="hero">
                <div className="hero-body">
                    <div className="container">
                        <h1 className="title"><img src={this.props.current_user.image_url} className="avater"/><div className="">{this.props.current_user.username}さんのページ</div></h1>
                    </div>
                </div>
            </section>
            <div className="container">
                <a className="bd-tw-button button" data-social-network="Twitter" data-social-action="tweet" data-social-target="http://tagmaru.me" target="_blank" href={"https://twitter.com/intent/tweet?text=私はたぐまるを使っています。" + this.props.follow_tags.map((tag) => (tag.name + "と"))+"タグをフォローしてます。 エンジニアのための情報収集サービス「たぐまる」http://tagmaru.me&amp;hashtags=tagmaru&amp;&amp;via=_anharu2"}>
                  <span className="icon">
                    <i className="fab fa-twitter"></i>
                  </span>
                  <span>
                    フォロしてるタグを共有
                  </span>
                </a>
            </div>

            <section>
                <div className="container">
                    <h1 className="title">フォロー中のタグ</h1>
                    <div>
            {this.props.follow_tags.map((tag) => (<Tag {...this.props} tag={tag} key={tag.id} from="follow_tags"/>))}
                    </div>
                </div>
            </section>
            </div>
        )
    }
}
