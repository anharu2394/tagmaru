import {
    REQUEST_FETCH_TAGS,
    FETCH_TAGS,
    FETCH_FOLLOW_TAGS,
    FETCH_USER,
    FETCH_TAG_DETAILS,
    CHANGE_FOLLOW_TAG,
    FETCH_POSTS,
    FETCH_TREND_POSTS,
    CHANGE_POST_TAB,
    SEND_MESSAGE,
    } from './../actions/AppActions'
import { combineReducers } from 'redux'

export const initialState = {
    tags: [],
    follow_tags: [],
    current_user: {},
    logged_in: false,
    home_trend_posts:[],
    latest_posts:[],
    popu_posts:[],
    trend_posts: [],
    isFetching: false,
    post_tab: "trend",
    detail_tag: {},
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
    case REQUEST_FETCH_TAGS: {
        if (action.status === "success") {
            return Object.assign({},state,{
                isFetching: false
            })
        }
        else {
            return Object.assign({},state,{
                error: action.error
            })
        }
    }
    case FETCH_TAGS: {
        if (action.status === "success") {
            return Object.assign({},state,{
                tags: action.response
            })
        }
        else {
            return Object.assign({},state,{
                error: action.error
            })
        }
    }
    case FETCH_FOLLOW_TAGS: {
        if (action.status === "success") {
            return Object.assign({},state,{
                follow_tags: action.response
            })
        }
        else {
            return Object.assign({},state,{
                error: action.error
            })
        }
    }
    case FETCH_USER: {
        if (action.status === "success") {
            return Object.assign({},state,{
                isFetching: true,
                current_user: action.response.user ? action.response.user : {},
                logged_in: action.response.logged_in
            })
        }
        else {
            return Object.assign({},state,{
                error: action.error
            })
        }
    }
    case FETCH_TAG_DETAILS: {
        if (action.status === "success") {
            return Object.assign({},state,{
                detail_tag: action.response
            })
        }
        else {
            return Object.assign({},state,{
                error: action.error
            })
        }
    }
    case CHANGE_FOLLOW_TAG: {
        if (action.status === "success") {
            const switchChangeState = (id,following,tag_state) => {
                const target_tag = tag_state.filter((tag) => {
                    return (tag.id === id)
                })[0]
                let changed_tag = target_tag
                changed_tag.following = following
                let result_tags = tag_state
                result_tags.splice(result_tags.indexOf(target_tag),changed_tag)
                return result_tags
            }
            const id = action.response.id
            const following = action.response.following
            if (action.response.from === "tags") {
                const result_tags = switchChangeState(id,following,state.tags.slice(0))
                return Object.assign({},state,{
                    tags: result_tags
                })
            }
            else if (action.response.from === "follow_tags") {
                const result_follow_tags = switchChangeState(id,following,state.follow_tags.slice(0))
                return Object.assign({},state,{
                    follow_tags: result_follow_tags
                })
            }
           // else if (action.response.from === "tag_search") {
            //    const result_follow_tags = switchChangeState(id,following,state.follow_tags.slice(0))
              //  return Object.assign({},state,{
              //      follow_tags: result_follow_tags
              //  })
           // }
            else {
                return state}
        }
        else {
            return Object.assign({},state,{
                error: action.error
            })
        }
    }
    case FETCH_POSTS: {
      if (action.status === "success") {
          if (action.tab === "trend") {
              return Object.assign({},state,{
                  trend_posts: action.response.posts
              })

          }
          else if (action.tab === "latest") {
              return Object.assign({},state,{
                  latest_posts: action.response.posts
              })

          }
          else if (action.tab == "popularity") {
              return Object.assign({},state,{
                  popu_posts: action.response.posts
              })

          }
      }
      else{
          return Object.assign({},state,{
              error: action.error
          })
      }
    }
    case FETCH_TREND_POSTS: {
      if (action.status === "success") {
          return Object.assign({},state,{
              home_trend_posts: action.response
          })
      }
      else{
          return Object.assign({},state,{
              error: action.error
          })
      }
    }
    case CHANGE_POST_TAB: {
      if (action.status === "success") {
          return Object.assign({},state,{
              post_tab: action.response
          })
      }
      else{
          return Object.assign({},state,{
              error: action.error
          })
      }
    }
    case SEND_MESSAGE: {
      if (action.status === "success") {
          return Object.assign({},state,{
              message: action.response
          })
      }
      else{
          return Object.assign({},state,{
              error: action.error
          })
      }
    }
    default: {
          return state
        }
    }
}

export default reducer
