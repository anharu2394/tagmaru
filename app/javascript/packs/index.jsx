// Run this example by adding <%= javascript_pack_tag 'hello_react' %> to the head of your layout file,
// like app/views/layouts/application.html.erb. All it does is render <div>Hello React</div> at the bottom
// of the page.

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension'
import { createBrowserHistory } from 'history'
import { createStore, compose, applyMiddleware } from 'redux'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import { Provider } from 'react-redux'
import App from './containers/App'
import reducer from './reducers/reducer'
import initialState from './reducers/reducer'
import { getTags, getFollowTag,  loginUser, } from './actions/AppActions'

const history = createBrowserHistory()
const composeEnhancers = composeWithDevTools({
  // Specify custom devTools options
})
let store
if (process.env.NODE_ENV !== `production`) {
    store = createStore(
        connectRouter(history)(reducer),
        composeEnhancers(
            applyMiddleware(
                routerMiddleware(history),
                thunk,
                logger,
            ),
        ),
        )
}
else {
    store = createStore(
        connectRouter(history)(reducer),
        compose(
            applyMiddleware(
                routerMiddleware(history),
                thunk,
            ),
        ),
        )

}

import PropTypes from 'prop-types'
import Header from './layouts/header'
import Home from './layouts/home'
import Footer from './layouts/footer'
import MyPage from './layouts/mypage'
import DetailTag from "./layouts/detail_tag"
import TagSearch from "./layouts/tag_search"
import axios from 'axios'
import {  Link  }from 'react-router'
import { BrowserRouter,Route,Switch } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'
import 'bulma/bulma.sass'
import '@fortawesome/fontawesome'
import '@fortawesome/fontawesome-free-solid'
import '@fortawesome/fontawesome-free-regular'
import '@fortawesome/fontawesome-free-brands'

document.addEventListener('DOMContentLoaded', () => {
    ReactDOM.render(
      <Provider store={store}>
          <ConnectedRouter history={history}>
              <App />
          </ConnectedRouter>
      </Provider>,
      document.getElementById('root')
    )
})
