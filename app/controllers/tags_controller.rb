class TagsController < ApplicationController
    def index
        @tags = Tag.all.first(20).map{ |t| t.attributes }
        user_tags = current_user.tags.map{ |t| t.attributes }
        @tags.map do |tag|
            if user_tags.index(tag) == nil
                tag.store("following",false)
            else
                tag.store("following",true)
            end
        end
        render json: @tags
    end
    def show
        @tag = Tag.find(params[:id])
        render json: @tag
    end
    def user_index
        @tags = User.find(params[:user_id]).tags.map{ |t| t.attributes }
        user_tags = current_user.tags.map{ |t| t.attributes }
        @tags.map do |tag|
            if user_tags.index(tag) == nil
                tag.store("following",false)
            else
                tag.store("following",true)
            end
        end
        render json: @tags
    end
    def search
        if params[:keyword] == ""
            render json: []
        elsif params[:keyword]
            @tags = Tag.where(['name LIKE ?', "%#{params[:keyword]}%"]).map{ |t| t.attributes }
            user_tags = current_user.tags.map{ |t| t.attributes }
            @tags.map do |tag|
                if user_tags.index(tag) == nil
                    tag.store("following",false)
                else
                    tag.store("following",true)
                end
            end
            render json: @tags
        else
            render json: []
        end
    end
end
