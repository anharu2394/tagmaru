class FollowTagsController < ApplicationController
    before_action :authenticate
    def create
        @follow_tag = FollowTag.create(user_id:current_user.id, tag_id: params[:tag_id])
        render json: @follow_tag
    end
    def destroy
        @follow_tag = FollowTag.where(tag_id:params[:id]).find_by(user_id:current_user.id)
        if @follow_tag.destroy
          render json: @follow_tag
        else
            render json: @follow_tag.errors, status: :unprocessable_entity
        end
    end
end
