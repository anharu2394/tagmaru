class UsersController < ApplicationController
    def current
        @user = User.find(current_user.id)
        render json: @user
    end
end
